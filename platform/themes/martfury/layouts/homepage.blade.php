{!! Theme::partial('header') !!}

<div id="homepage-1 home-main">
    {{--{!! Theme::content() !!}--}}
    <div class="category-listing">
        <div class="categories-boxes"><img alt="1" border="0" src="https://i.ibb.co/vdvY15w/1.jpg" /> <span> Hair Care</span></div>
        <div class="categories-boxes"><img alt="" src="https://i.ibb.co/GQypDvZ/2.jpg" /> <span> Skin Care</span></div>
        <div class="categories-boxes"><img alt="" src="https://i.ibb.co/nk0C5N6/3.jpg" /> <span> Face Care</span></div>
        <div class="categories-boxes"><img alt="" src="https://i.ibb.co/pdZdmn6/4.jpg" /> <span> Men Care</span></div>
        <div class="categories-boxes"><img alt="" src="https://i.ibb.co/v1wGk2t/5.jpg" /> <span> Children</span></div>
        <div class="categories-boxes"><img alt="" src="https://i.ibb.co/WzP83y9/6.jpg" /> <span> Extension</span></div>
        <div class="categories-boxes"><img alt="" src="https://i.ibb.co/dp7cvDJ/7.jpg" /> <span> Electric</span></div>
        <div class="categories-boxes"><img alt="" src="https://i.ibb.co/TMhpFx0/8.jpg" /> <span> Accessories</span></div>
    </div>
    <div class="welcome-section">
        <h2>Welcome to Black Beauty Online.</h2>
        <p>Your one stop shop for all your hair and beauty needs. Feel free to indulge yourself in our wide variety of products.</p>
        <img alt="" src="https://i.ibb.co/zfPybcM/2.jpg" />
    </div>
    <div class="welcome-section covid-update">
        <h2>COVID-19 Store Update</h2>
        <p>We are shipping orders as normal by Royal Mail. Please allow an additional 1-2 days if your order is not delivered in the given time frame. If possible, please select the &#39;Pick Up&#39; option upon check out so you can get your goods as early as possible.</p>
        <div class="product-types">
            <div class="product-image">
                <img alt="" src="https://i.ibb.co/BN0hdZh/4.png" /> <span>Plaits &amp; Crochet Hair </span>
                <p>We stock a wide range of braiding hair which come in all sorts of shapes, sizes and colours.</p>
            </div>
            <div class="product-image">
                <img alt="" src="https://i.ibb.co/8KKK8KV/6.png" /> <span>Hair Caps &amp; Bonnets </span>
                <p>Check out our extensive range of caps &amp; bonnets.</p>
            </div>
            <div class="product-image">
                <img alt="" src="https://i.ibb.co/nm9vWcy/5.png" /> <span>Hair Dye </span>
                <p>All your favourite colours at your favourite prices.</p>
            </div>
        </div>
    </div>
    <div class="store-collection">
        <div class="store-info">
            <h4>Our Collection Store</h4>
            <p>Black Beauty Cosmetics <br> 9 Catford Broadway <br> SE6 4SP <br> London</p>
            <p>Mon - Sun, 10am - 5pm</p>
            <button>Get Direction</button>
        </div>
    </div>
</div>

{!! Theme::partial('footer') !!}
